#!/usr/bin/env python

# http://docs.python-requests.org/en/latest/user/quickstart/

import sys, requests

if len(sys.argv) < 2:
	print 'Usage: ./bomberman.py <file.zip>'
	exit()

# print sys.argv[1]; exit()

url = 'http://dev.roskanal.ru/bitrix/admin/1c_exchange.php'

auth = ('admin', 'p@ssw0rd')
params = {'type': 'catalog', 'mode': 'checkauth'}

# 1st step
r = requests.get(url, params=params, auth=auth)

print 'done step 1'

lines = r.text.splitlines()

for l in lines:
	if l.startswith('sessid'):
		params['sessid'] = l[7:]

params['mode'] = 'init'
params['v'] = '2.10'

#for c in r.cookies:
#	print c

cookies = r.cookies

# print r.cookies['PHPSESSID']

# 2nd step
r = requests.get(url, params=params, cookies=cookies)

print 'done step 2'
#print r.text

# 3rd step - file upload
#files = {'file': open('ololo.3.zip', 'rb')}
params['mode'] = 'file'
params['filename'] = sys.argv[1] # 'import___0f47c74e-ab4e-4be1-b1d4-fcf83c92729f.zip'

del params['v']

# print params

f = open(params['filename'], 'rb')

data = f.read()

r = requests.post(url, params=params, cookies=cookies, data=data)

# print r.text

print 'done step 3'

# print cookies
# print params

# now import
params['mode'] = 'import'
params['filename'] = params['filename'].replace('.zip', '.xml')

r = requests.get(url, params=params, cookies=cookies)

resp_code = r.text.splitlines()[0]
resp_message = r.text.splitlines()[1]

print '%s > %s' % (resp_code, resp_message)

while u'progress' == resp_code:
	r = requests.get(url, params=params, cookies=cookies)

	resp_code = r.text.splitlines()[0]
	resp_message = r.text.splitlines()[1]

	print '%s > %s' % (resp_code, resp_message)


